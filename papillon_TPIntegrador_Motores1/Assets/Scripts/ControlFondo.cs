using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlFondo : MonoBehaviour
{
    public float velocidad = 10f; // Velocidad de movimiento en unidades por segundo

    void Update()
    {
        // Sumar 10 unidades al eje Z del objeto basado en el tiempo
        float movimientoEnZ = velocidad * Time.deltaTime;

        // Obtener la posici�n actual del objeto
        Vector3 posicionActual = transform.position;

        // Sumar el movimiento al eje Z
        posicionActual.z += movimientoEnZ;

        // Aplicar la nueva posici�n al objeto
        transform.position = posicionActual;
    }
}
