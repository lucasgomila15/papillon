using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlRotacionMenu : MonoBehaviour
{
    private float velocidadRotacion = 15f;
    public bool girar;

    private void Start()
    {
        girar = true;
    }

    void Update()
    {
        if (girar)
        {
            transform.Rotate(Vector3.up, velocidadRotacion * Time.deltaTime);
        }
        else if (!girar)
        {
            transform.rotation = Quaternion.identity;
            gameObject.SetActive(false);
        }
    }
}
