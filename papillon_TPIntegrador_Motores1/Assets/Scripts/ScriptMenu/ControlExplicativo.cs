using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ControlExplicativo : MonoBehaviour
{
    ControlMenuPrincipal controlMenuPrincipal;
    ControlRotacionMenu controlRotacionMenu;

    public float tutorial;
    public GameObject canva;
    public GameObject MenuPrincial;
    public GameObject MenuExplicativo;
    public GameObject EmptyXbox;
    public GameObject EmptyTeclado;
    [SerializeField] TextMeshProUGUI jugar;
    [SerializeField] TextMeshProUGUI salir;
    [SerializeField] GameObject seguimiento;
    Vector3 stickIzquierdo = Vector3.zero;
    private float seleccion;

    // Start is called before the first frame update
    void Start()
    {
        controlMenuPrincipal = MenuPrincial.GetComponent<ControlMenuPrincipal>();
        controlRotacionMenu = seguimiento.GetComponent<ControlRotacionMenu>();
        tutorial = 0;
        seleccion = 1;
        jugar.color = Color.red;
        salir.color = Color.grey;
    }

    // Update is called once per frame
    void Update()
    {
        stickIzquierdo.x = Input.GetAxis("DPadX");
        stickIzquierdo.y = Input.GetAxis("DPadY");

        if (Input.GetMouseButtonDown(0) && tutorial == 1)
        {
            MenuPrincial.gameObject.SetActive(false);
            MenuExplicativo.gameObject.SetActive(true);
            EmptyTeclado.gameObject.SetActive(true);
            EmptyXbox.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button0) && tutorial == 1)
        {
            MenuPrincial.gameObject.SetActive(false);
            MenuExplicativo.gameObject.SetActive(true);
            EmptyTeclado.gameObject.SetActive(false);
            EmptyXbox.gameObject.SetActive(true);
        }

        if (stickIzquierdo.x > 0f)
        {
            jugar.color = Color.grey;
            salir.color = Color.red;
            seleccion = 0;
        }
        if (stickIzquierdo.x < 0f)
        {
            salir.color = Color.grey;
            jugar.color = Color.red;
            seleccion = 1;
        }

        if ((Input.GetKey(KeyCode.E) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && seleccion == 1)
        {
            tutorial = 1;
            controlRotacionMenu.girar = false;
            MenuPrincial.gameObject.SetActive(false);
            MenuExplicativo.gameObject.SetActive(true);
            EmptyTeclado.gameObject.SetActive(false);
            EmptyXbox.gameObject.SetActive(true);
        }
        if ((Input.GetKey(KeyCode.E) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && seleccion == 0)
        {
            controlMenuPrincipal.Salir();
        }

        if ((Input.GetKey(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button7)) && tutorial == 1)
        {
            GestorDeAudio.instancia.PausarSonido("MusicaMenu");
            MenuPrincial.gameObject.SetActive(true);
            MenuExplicativo.gameObject.SetActive(false);
            SceneManager.LoadScene(1);
        }
    }
}
