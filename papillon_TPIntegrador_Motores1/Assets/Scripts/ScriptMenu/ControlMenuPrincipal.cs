using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ControlMenuPrincipal : MonoBehaviour
{
    ControlRotacionMenu controlRotacionMenu;
    ControlExplicativo controlExplicativo;
    [SerializeField] GameObject canva;
    [SerializeField] GameObject seguimiento;

    private void Start()
    {
        controlExplicativo = canva.GetComponent<ControlExplicativo>();
        controlRotacionMenu = seguimiento.GetComponent<ControlRotacionMenu>();
    }

    public void Jugar()
    {
        controlExplicativo.tutorial = 1;
        controlRotacionMenu.girar = false;
    }

    public void Salir()
    {
        Debug.Log("Salir");
        Application.Quit();
    }
}
