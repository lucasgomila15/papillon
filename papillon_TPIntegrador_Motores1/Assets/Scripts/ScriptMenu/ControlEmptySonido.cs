using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEmptySonido : MonoBehaviour
{
    private bool completoNivel;

    void Start()
    {
        completoNivel = false;
        GestorDeAudio.instancia.ReproducirSonido("MusicaMenu");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.F))
        {
            completoNivel = true;
        }
        if (Input.GetKey(KeyCode.P) && completoNivel)
        {
            GestorDeAudio.instancia.PausarSonido("MusicaMenu");
            GestorDeAudio.instancia.ReproducirSonido("MusicaPapillon");
        }
    }
}
