using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    ControlManagerJugador controlManagerJugador;
    [SerializeField] GameObject jugador;
    [SerializeField] GameObject canva1;
    [SerializeField] GameObject canva2;
    [SerializeField] GameObject camara;
    [SerializeField] GameObject nivel1;
    public List<GameObject> arosNivel1 = new List<GameObject>();
    public List<GameObject> arosNivel2 = new List<GameObject>();
    public List<GameObject> arosNivel3 = new List<GameObject>();
    public List<GameObject> enemigosNivel1 = new List<GameObject>();
    public List<GameObject> enemigosNivel2 = new List<GameObject>();
    public List<GameObject> enemigosNivel3 = new List<GameObject>();
    [SerializeField] GameObject nivel1Paredes;
    [SerializeField] GameObject nivel2;
    [SerializeField] GameObject nivel2Paredes;
    [SerializeField] GameObject nivel3;
    [SerializeField] GameObject nivel3Paredes;
    public TextMeshProUGUI vida;
    public TextMeshProUGUI reloj;
    public TextMeshProUGUI perdiste;
    public TextMeshProUGUI contador;
    public TextMeshProUGUI pasarNivel;
    public TextMeshProUGUI nivel;
    private float tiempoRestante;
    private float tiempo = 60;
    private bool contadorActivo = false;
    public bool canva2Activado;
    public bool irNivel;
    public bool tieneHabilidad;
    public bool nuevoJuego;
    public bool gano;

    void Start()
    {
        irNivel = false;
        controlManagerJugador = jugador.GetComponent<ControlManagerJugador>();

        controlManagerJugador.vidaJugador = 100;
        jugador.gameObject.SetActive(false);
        nivel1.gameObject.SetActive(false);
        nivel2.gameObject.SetActive(false);
        nivel3.gameObject.SetActive(false);
        camara.gameObject.SetActive(false);

        controlManagerJugador.nivel = 0;
        nuevoJuego = false;

        terminarNiveles();
        reinciarHabilidad();

        /*
        comenzarNivel1();
        nivel.text = "NIVEL 1";
        GestorDeAudio.instancia.ReproducirSonido("MusicaNivel1");
        */
    }

    void Update()
    {
        setearTextos();

        if (controlManagerJugador.canva2Activado == true)
        {
            irNivel = false; 
            terminarNiveles();
            cerrarCanva1();
            mostrarCanva2();
        }
        if (controlManagerJugador.canva2Activado == false)
        {
            irNivel = true;
        }

        if(tieneHabilidad && !nuevoJuego)
        {
            nuevoJuego = true;
            terminarNiveles();
            comenzarNivel1();
            nivel.text = "NIVEL 1";
            GestorDeAudio.instancia.ReproducirSonido("MusicaNivel1");
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button1) && tieneHabilidad == false)
        {
            escudo();
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button2) && tieneHabilidad == false)
        {
            bala2();
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button3) && tieneHabilidad == false)
        {
            vidaMas();
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button7)){
            controlManagerJugador.habilidadNumero = 0;
            tieneHabilidad = false;
        }



        if ((Input.GetKey(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && !contadorActivo && controlManagerJugador.nivel == 0 && irNivel && tieneHabilidad)
        {
            terminarNiveles();
            comenzarNivel1();
            nivel.text = "NIVEL 1";
            GestorDeAudio.instancia.ReproducirSonido("MusicaNivel1");
            gano = false;
        }
        if ((Input.GetKey(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && !contadorActivo && controlManagerJugador.nivel == 1 && irNivel && tieneHabilidad)
        {
            terminarNiveles();
            comenzarNivel2();
            nivel.text = "NIVEL 2";
            GestorDeAudio.instancia.ReproducirSonido("MusicaNivel2");
            gano = false;
        }
        if ((Input.GetKey(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && !contadorActivo && controlManagerJugador.nivel == 2 && irNivel && tieneHabilidad)
        {
            terminarNiveles();
            comenzarNivel3();
            nivel.text = "NIVEL FINAL";
            GestorDeAudio.instancia.ReproducirSonido("MusicaNivel3");
            gano = false;
        }
        if ((Input.GetKey(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && !contadorActivo && controlManagerJugador.nivel == 3 && irNivel)
        {
            controlManagerJugador.nivel = 0;
            VolverMenu();
            gano = false;
        }

        if ((Input.GetKey(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && !contadorActivo && irNivel && !tieneHabilidad)
        {
            terminarNiveles();
            controlManagerJugador.canva2Activado = true;
            gano = false;
        }


        //Si es que murio
        if (controlManagerJugador.estado == "Muerto" && contadorActivo && !gano)
        {
            jugador.gameObject.SetActive(false);
            desactivarEnemigos();
            camara.gameObject.SetActive(false);
            perdiste.text = "YOU LOSE";
            pasarNivel.text = "APRETA A PARA VOLVER A INTENTARLO";
            controlManagerJugador.estado = "Vivo";
            StartCoroutine(ComenzarCronometro(0));
        }
        //LLego al tiempo pero no al puntaje
        if (tiempo <= 0 && controlManagerJugador.puntaje < 10 && !contadorActivo && !gano)
        {
            jugador.gameObject.SetActive(false);
            desactivarEnemigos();
            camara.gameObject.SetActive(false);
            perdiste.text = "GAME OVER";
            pasarNivel.text = "APRETA A PARA VOLVER A INTENTARLO";
            StartCoroutine(ComenzarCronometro(0));
        }
        //Si es que Gano
        if (tiempo <= 0 && controlManagerJugador.puntaje >= 10 && !contadorActivo)
        {
            jugador.gameObject.SetActive(false);
            desactivarEnemigos();
            camara.gameObject.SetActive(false);
            perdiste.text = "YOU WIN";
            pasarNivel.text = "APRETA A PARA PASAR DE NIVEL";
            tieneHabilidad = false;
            gano = true;
            StartCoroutine(ComenzarCronometro(0));
        }
        if(tiempo <= 0 && controlManagerJugador.puntaje < 10 && !contadorActivo && gano)
        {
            jugador.gameObject.SetActive(false);
            desactivarEnemigos();
            camara.gameObject.SetActive(false);
            perdiste.text = "YOU WIN";
            pasarNivel.text = "APRETA A PARA PASAR DE NIVEL";
        }

        //Ya gano
        if (tiempo <= 0 && controlManagerJugador.puntaje >= 10 && !contadorActivo && controlManagerJugador.nivel == 3)
        {
            jugador.gameObject.SetActive(false);
            desactivarEnemigos();
            camara.gameObject.SetActive(false);
            perdiste.text = "THE END";
            pasarNivel.text = "APRETA A PARA VOLVER AL MENU";
            gano = true;
            StartCoroutine(ComenzarCronometro(0));
        }

        if (Input.GetKey(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button7))
        {
            jugador.gameObject.SetActive(false);
            VolverMenu();
        }
    }

    private void setearTextos()
    {
        vida.text = "HP RESTANTE: " + controlManagerJugador.vidaJugador.ToString();
        reloj.text = tiempoRestante.ToString();
        contador.text = "PUNTAJE: " + controlManagerJugador.puntaje.ToString();
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        contadorActivo = true;
        tiempoRestante = valorCronometro;
        while(tiempoRestante > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoRestante -= 1;
            tiempo -= 1;
        }

        contadorActivo = false;
    }

    void comenzarNivel1()
    {
        jugador.gameObject.transform.position = new Vector3(3.52f, 11.71f, -18.27f);
        nivel1Paredes.gameObject.transform.position = new Vector3(0f, 0f, 0f);
        jugador.gameObject.SetActive(true);
        nivel1.gameObject.SetActive(true);
        camara.gameObject.SetActive(true);
        activarEnemigos();
        controlManagerJugador.puntaje = 0;
        controlManagerJugador.vidaJugador = 100;
        activarAros();
        StartCoroutine(ComenzarCronometro(60));
    }

    void terminarNiveles()
    {
        camara.gameObject.SetActive(false);
        perdiste.text = " ";
        pasarNivel.text = " ";
        nivel1.gameObject.SetActive(false);
        nivel2.gameObject.SetActive(false);
    }

    void comenzarNivel2()
    {
        jugador.gameObject.transform.position = new Vector3(3.52f, 11.71f, -18.27f);
        nivel2Paredes.gameObject.transform.position = new Vector3(0f, 0f, 0f);
        jugador.gameObject.SetActive(true);
        nivel2.gameObject.SetActive(true);
        camara.gameObject.SetActive(true);
        activarEnemigos();
        controlManagerJugador.puntaje = 0;
        controlManagerJugador.vidaJugador = 100;
        activarAros();
        StartCoroutine(ComenzarCronometro(60));
    }
    void comenzarNivel3()
    {
        jugador.gameObject.transform.position = new Vector3(3.52f, 11.71f, -18.27f);
        nivel3Paredes.gameObject.transform.position = new Vector3(0f, 0f, 0f);
        jugador.gameObject.SetActive(true);
        nivel3.gameObject.SetActive(true);
        camara.gameObject.SetActive(true);
        activarEnemigos();
        controlManagerJugador.puntaje = 0;
        controlManagerJugador.vidaJugador = 100;
        activarAros();
        StartCoroutine(ComenzarCronometro(60));
    }

    void activarAros()
    {
        if (controlManagerJugador.nivel == 0)
        {
            foreach (GameObject obj in arosNivel1)
            {
                obj.SetActive(true);
            }
        }
        else if (controlManagerJugador.nivel == 1)
        {
            foreach (GameObject obj in arosNivel2)
            {
                obj.SetActive(true);
            }
        }else if (controlManagerJugador.nivel == 2)
        {
            foreach (GameObject obj in arosNivel3)
            {
                obj.SetActive(true);
            }
        }
    }
    void activarEnemigos()
    {
        if (controlManagerJugador.nivel == 0)
        {
            foreach (GameObject obj in enemigosNivel1)
            {
                obj.SetActive(true);
            }
        }
        else if(controlManagerJugador.nivel == 1)
        {
            foreach (GameObject obj in enemigosNivel2)
            {
                obj.SetActive(true);
            }
        }
        else if(controlManagerJugador.nivel == 2)
        {
            foreach (GameObject obj in enemigosNivel3)
            {
                obj.SetActive(true);
            }
        }
    }
    void desactivarEnemigos()
    {
        if(controlManagerJugador.nivel == 0)
        {
            foreach (GameObject obj in enemigosNivel1)
            {
                obj.SetActive(false);
            }
        }
        else if(controlManagerJugador.nivel == 1)
        {
            foreach (GameObject obj in enemigosNivel2)
            {
                obj.SetActive(false);
            }
        }
        else if (controlManagerJugador.nivel == 2)
        {
            foreach (GameObject obj in enemigosNivel3)
            {
                obj.SetActive(false);
            }
        }
    }

    public void VolverMenu()
    {
        GestorDeAudio.instancia.PararSonido("MusicaNivel1");
        GestorDeAudio.instancia.PararSonido("MusicaNivel2");
        GestorDeAudio.instancia.PararSonido("MusicaNivel3");
        SceneManager.LoadScene(0);
    }

    public void mostrarCanva1()
    {
        canva1.gameObject.SetActive(true);
    }
    public void cerrarCanva1()
    {
        canva1.gameObject.SetActive(false);
    }

    public void mostrarCanva2()
    {
        canva2.gameObject.SetActive(true);
    }
    public void cerrarCanva2()
    {
        canva2.gameObject.SetActive(false);
    }

    public void escudo()
    {
        controlManagerJugador.habilidadNumero = 1;
        cerrarCanva2();
        mostrarCanva1();
        controlManagerJugador.canva2Activado = false;
        controlManagerJugador.puntaje = 0;
        tieneHabilidad = true;
    }
    public void bala2()
    {
        controlManagerJugador.habilidadNumero = 2;
        cerrarCanva2();
        mostrarCanva1();
        controlManagerJugador.canva2Activado = false;
        controlManagerJugador.puntaje = 0;
        tieneHabilidad = true;
    }
    public void vidaMas()
    {
        controlManagerJugador.habilidadNumero = 3;
        cerrarCanva2();
        mostrarCanva1();
        controlManagerJugador.canva2Activado = false;
        controlManagerJugador.puntaje = 0;
        tieneHabilidad = true;
    }
    public void reinciarHabilidad()
    {
        controlManagerJugador.habilidadNumero = 0;
        cerrarCanva1();
        mostrarCanva2();
        controlManagerJugador.canva2Activado = false;
        controlManagerJugador.puntaje = 0;
        tieneHabilidad = false;
    }
}

