using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAro : MonoBehaviour
{
    public GameObject bala;
    Cinemachine.CinemachineImpulseSource source;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            movimientoCamara();
            gameObject.SetActive(false);
        }
    }

    public void movimientoCamara()
    {
        source = GetComponent<Cinemachine.CinemachineImpulseSource>();

        source.GenerateImpulse(Camera.main.transform.forward);
    }
   
}
