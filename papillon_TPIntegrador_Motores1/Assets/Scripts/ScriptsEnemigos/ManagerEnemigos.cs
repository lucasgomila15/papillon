using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerEnemigos : MonoBehaviour
{
    ControlManagerJugador controlManagerJugador;
    private bool muerto;
    public GameObject shader;
    public GameObject jugador;
    DisparoEnemigos disparoEnemigos;

    public Image barra;
    public float daņoRecibido = 0.5f;
    public float X;
    public float Y;
    public float Z;
    public float hitsRecibir;
    public string tipoEnemigo;
    private float velocidadDescenso = 8f;
    private float velocidadGiro = 20f;
    public float vida;

    private void Start()
    {
        controlManagerJugador = jugador.GetComponent<ControlManagerJugador>();
        vida = hitsRecibir;
        transform.Rotate(Vector3.right, 0f);
        disparoEnemigos = gameObject.GetComponent<DisparoEnemigos>();
    }

    void Update()
    {
        if (tipoEnemigo == "NaveGrande" && hitsRecibir <= 0)
        {
            controlManagerJugador.mejora = true;
            muerto = true;
            shader.gameObject.SetActive(true);
            hitsRecibir = vida;
        }

        if(tipoEnemigo == "Cohete" && hitsRecibir <= 0)
        {
            controlManagerJugador.mejora = true;
            muerto = true;
            shader.gameObject.SetActive(true);
            hitsRecibir = vida;
            disparoEnemigos.rapidez = 0;
        }

        if (muerto)
        {
            Vector3 nuevaPosicion = transform.position - Vector3.up * velocidadDescenso * Time.deltaTime;
            Vector3 desplazamiento = -Vector3.up * velocidadDescenso * Time.deltaTime;
            float giro = velocidadGiro * Time.deltaTime;
            transform.position = nuevaPosicion;
            transform.Translate(desplazamiento);
            transform.Rotate(Vector3.right, giro);

        }

        if (gameObject.transform.position.y <= -10)
        {
            hitsRecibir = vida;
            barra.fillAmount = vida;
            shader.gameObject.SetActive(false);
            gameObject.SetActive(false);
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            muerto = false;
            gameObject.transform.position = new Vector3(X, Y, Z);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            hitsRecibir--;
            barra.fillAmount -= daņoRecibido;
        }
        if (collision.gameObject.CompareTag("Bala2"))
        {
            hitsRecibir -= 2;
            barra.fillAmount -= daņoRecibido * 2;
        }
    }
}
