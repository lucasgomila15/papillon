using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlNaveCarga : MonoBehaviour
{
    public GameObject jugador;
    public float velocidad;
    public float distancia;
    public float distanciaMinimaJugador;
    public float X;
    public float Y;
    public float Z;

    private void Start()
    {
        gameObject.transform.position = new Vector3(X, Y, Z);
    }

    void Update()
    {
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.transform.position);

        if (distanciaAlJugador <= distanciaMinimaJugador)
        {
            transform.Translate(-Vector3.forward * velocidad * Time.deltaTime);

            if (transform.position.z <= distancia)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
