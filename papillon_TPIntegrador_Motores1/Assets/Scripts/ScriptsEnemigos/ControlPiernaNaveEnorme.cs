using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPiernaNaveEnorme : MonoBehaviour
{
    private Rigidbody rb;
    private Vector3 posicionInicial;
    private bool caidaPie = false;
    public float velocidadSubida = 5f;

    public GameObject jugador;
    public float distanciaMinimaJugador;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        posicionInicial = transform.position;
    }

    void Update()
    {
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.transform.position);

        if (distanciaAlJugador <= distanciaMinimaJugador)
        {
            Invoke("activacionGravedad", Random.Range(2f, 6f));

            if (caidaPie)
            {
                if (transform.position.y <= 0f)
                {
                    rb.velocity = Vector3.zero;
                    rb.useGravity = false;
                    caidaPie = false;
                }
            }
            else
            {
                float step = velocidadSubida * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, posicionInicial, step);
            }
        }
    }
    void activacionGravedad()
    {
        rb.useGravity = true;
        caidaPie = true;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            float step = velocidadSubida * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, posicionInicial, step);
        }
    }
}
