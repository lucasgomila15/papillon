using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCohete : MonoBehaviour
{
    public GameObject jugador;
    public GameObject torreta;
    public int rapidez;
    public float distanciaMaxima;
    public float vida;

    void Update()
    {
        Vector3 direccionAlJugador = jugador.transform.position - transform.position;

        direccionAlJugador.y = 0f;

        float distanciaAlJugador = direccionAlJugador.magnitude;

        if(distanciaAlJugador <= distanciaMaxima)
        {
            gameObject.SetActive(true);
            transform.LookAt(jugador.transform);
            transform.Rotate(0f, 0f, 0f);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
        if(distanciaAlJugador >= distanciaMaxima)
        {
            transform.LookAt(gameObject.transform);
            vida = 2;
        }

        if (vida <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            vida--;
        }
        if (collision.gameObject.CompareTag("Bala2"))
        {
            vida -= 2;
        }
    }
}
