using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisparoEnemigos : MonoBehaviour
{
    public GameObject jugador;
    public Transform objetivo;
    ManagerEnemigos managerEnemigos;
    public GameObject shader;
    public Image barra;
    public int rapidez;
    public float distanciaMinima;
    public float distanciaMaxima;
    public float X;
    public float Y;
    public float Z;
    public GameObject balaPrefab;
    public Transform puntoDisparo;
    public float velocidadBala = 10f;
    public float distanciaDisparo = 10f;
    private float tiempoUltimoDisparo;
    public float timerDisparo;

    private void Start()
    {
        managerEnemigos = gameObject.GetComponent<ManagerEnemigos>();
    }

    void Update()
    {
        Vector3 direccionAlJugador = jugador.transform.position - transform.position;

        direccionAlJugador.y = 0f;

        float distanciaAlJugador = direccionAlJugador.magnitude;
        float distanciaAlObjetivo = Vector3.Distance(transform.position, objetivo.position);

        if (distanciaAlJugador <= distanciaMinima)
        {
            direccionAlJugador.Normalize();

            transform.LookAt(jugador.transform);
            transform.Rotate(0f, 0f, 0f);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

            if (distanciaAlObjetivo <= distanciaDisparo && Time.time - tiempoUltimoDisparo >= timerDisparo)
            {
                Disparar();
                tiempoUltimoDisparo = Time.time;
            }
        }

        if (distanciaAlJugador >= distanciaMinima)
        {
            managerEnemigos.vida = managerEnemigos.hitsRecibir;
            transform.LookAt(gameObject.transform);
            gameObject.transform.position = new Vector3(X, Y, Z);
            transform.Rotate(Vector3.right , 0f);
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            barra.fillAmount = managerEnemigos.vida;
        }
    }
    private void Disparar()
    {
        GameObject bala = Instantiate(balaPrefab, puntoDisparo.position, transform.rotation);
        Rigidbody rb = bala.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * velocidadBala;
        Destroy(bala, 2f);
        GestorDeAudio.instancia.ReproducirSonido("SonidoDisparoEnemigo");
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Fondo"))
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            shader.gameObject.SetActive(false);
            gameObject.SetActive(false);
            gameObject.transform.position = new Vector3(X, Y, Z);
            barra.fillAmount = managerEnemigos.vida;
        }
    }
}
