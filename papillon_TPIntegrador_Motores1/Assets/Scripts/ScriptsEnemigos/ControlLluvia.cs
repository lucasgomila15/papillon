using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlLluvia : MonoBehaviour
{
    public GameObject balaPrefab;
    public GameObject jugador;
    public Transform puntoDeDisparo;
    public int cantidadDeBalas;
    public float velocidadBala;
    public float distanciaMinimaJugador;

    private bool desactivado;

    private void Start()
    {
        desactivado = false;
    }

    void Update()
    {
        float distanciaAlJugador = Vector3.Distance(transform.position, jugador.transform.position);

        if (distanciaAlJugador <= distanciaMinimaJugador && !desactivado)
        {
            Disparar();
            desactivado = true;
        }
    }

    void Disparar()
    {
        for (int i = 0; i < cantidadDeBalas; i++)
        {
            GameObject bala = Instantiate(balaPrefab, puntoDeDisparo.position, puntoDeDisparo.rotation);
            Rigidbody2D rb = bala.GetComponent<Rigidbody2D>();
            rb.velocity = puntoDeDisparo.forward * velocidadBala;
            Destroy(bala, 1f);
        }
    }
}
