using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMovimientoJugador : MonoBehaviour
{
    private Rigidbody rb;
    public GameObject particulas;

    public float velocidad;
    public float velocidadVertical;
    public float frenado;
    public float inclinacionMaxima = 20f;
    public float velocidadRotacion = 2f;
    private bool aumentarVelocidad;
    private float duracion = 6f;
    private float temporizador;
    private bool doblePosible = true;
    public float distanciaMovimiento = 7f;

    Vector3 stickIzquierdo = Vector3.zero;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        aumentarVelocidad = false;
    }

    void Update()
    {
        rb.velocity = transform.forward * velocidad;
        Vector3 movimiento = rb.velocity;

        stickIzquierdo.x = Input.GetAxis("Horizontal");
        stickIzquierdo.y = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.W) || stickIzquierdo.y > 0f)
        {
            movimiento += transform.up * velocidadVertical;
        }
        if (Input.GetKey(KeyCode.S) || stickIzquierdo.y < 0f)
        {
            movimiento -= transform.up * velocidadVertical;
        }
        if (Input.GetKey(KeyCode.D) || stickIzquierdo.x > 0f)
        {
            movimiento += transform.right * velocidad;
            Quaternion targetRotation = Quaternion.Euler(0f, 0f, -inclinacionMaxima);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, velocidadRotacion * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.A) || stickIzquierdo.x < 0f)
        {
            movimiento -= transform.right * velocidad;
            Quaternion targetRotation = Quaternion.Euler(0f, 0f, inclinacionMaxima);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, velocidadRotacion * Time.deltaTime);
        } else
        {
            Quaternion targetRotation = Quaternion.Euler(0f, 0f, 0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, velocidadRotacion * Time.deltaTime);
        }
        if ((Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.Joystick1Button5)) && doblePosible)
        {
            particulas.gameObject.SetActive(true);
            transform.position = new Vector3(transform.position.x + 5f, transform.position.y, transform.position.z);
            StartCoroutine(HacerAccionYEsperar());
            GestorDeAudio.instancia.ReproducirSonido("SonidoTP");
        }
        if ((Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.Joystick1Button4)) && doblePosible)
        {
            particulas.gameObject.SetActive(true);
            transform.position = new Vector3(transform.position.x - 5f, transform.position.y, transform.position.z);
            StartCoroutine(HacerAccionYEsperar());
            GestorDeAudio.instancia.ReproducirSonido("SonidoTP");
        }
        rb.velocity = movimiento;
        if (Input.GetMouseButtonDown(1) || Input.GetKey(KeyCode.Joystick1Button2))
        {
            rb.velocity *= frenado;
        }
        if (aumentarVelocidad)
        {
            temporizador += Time.deltaTime;
            if (temporizador >= duracion)
            {
                aumentarVelocidad = false;
                temporizador = 0f;
                velocidad -= 5;
            }
        }

    }
    void OnCollisionEnter(Collision collision)
    {   
        if (collision.gameObject.CompareTag("Aro"))
        {
            aumentarVelocidad = true;
            velocidad += 5;
        }
        if (collision.gameObject.CompareTag("AroFinal"))
        {
            aumentarVelocidad = false;
            temporizador = 0f;
            velocidad = 10;
        }
        if (collision.gameObject.CompareTag("AroReinicio"))
        {
            aumentarVelocidad = false;
            temporizador = 0f;
            velocidad = 10;
        }
        if (collision.gameObject.CompareTag("Respawn"))
        {
            aumentarVelocidad = true;
            temporizador = 0f;
            velocidad = 10;
        }
    }
    IEnumerator HacerAccionYEsperar()
    {
        doblePosible = false;

        yield return new WaitForSeconds(3f);

        doblePosible = true;
    }
}
