using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ControlDisparoJugador : MonoBehaviour
{
    ControlManagerJugador controlManagerJugador;
    public GameObject bala;
    public GameObject bala2;
    public TextMeshProUGUI tipoBala;
    public string seleccion = "Bala";
    public bool especial = false;
    public Transform puntoDisparo;
    public float fuerzaDisparo = 50f;

    private void Start()
    {
        tipoBala.text = "BALA: LASER";
    }

    void Update()
    {
        if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && seleccion == "Bala" && !especial)
        {
            dispararBala();
        }
        else if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && seleccion == "Bala2" && especial)
        {
            dispararBala2();
        }
        else if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Joystick1Button0)))
        {
            dispararBala();
            especial = false;
        }
    }

    void dispararBala()
    {
        GameObject bullet = Instantiate(bala, puntoDisparo.position, puntoDisparo.rotation);

        Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
        bulletRb.AddForce(puntoDisparo.forward * fuerzaDisparo, ForceMode.Impulse);

        Destroy(bullet, 2f);
        GestorDeAudio.instancia.ReproducirSonido("SonidoDisparo");
    }
    void dispararBala2()
    {
        GameObject bullet = Instantiate(bala2, puntoDisparo.position, puntoDisparo.rotation);

        Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
        bulletRb.AddForce(puntoDisparo.forward * fuerzaDisparo, ForceMode.Impulse);

        Destroy(bullet, 2f);
        GestorDeAudio.instancia.ReproducirSonido("SonidoDisparo");
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("AroReinicio"))
        {
            seleccion = "Bala";
            fuerzaDisparo = 50f;
            tipoBala.text = "BALA: LASER";
        }
        if (collision.gameObject.CompareTag("AroBala"))
        {
            especial = true;
            seleccion = "Bala2";
            fuerzaDisparo = 50f;
            tipoBala.text = "BALA: ROCA";
        }
        if (collision.gameObject.CompareTag("AroFinal"))
        {
            seleccion = "Bala";
            fuerzaDisparo = 30f;
        }
    }
}
