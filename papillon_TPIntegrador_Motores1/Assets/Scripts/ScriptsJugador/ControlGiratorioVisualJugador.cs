using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGiratorioVisualJugador : MonoBehaviour
{
    public float inclinacionMaxima = 45f;
    public float velocidadRotacion = 5f;
    Vector3 stickIzquierdo = Vector3.zero;

    void Update()
    {
        stickIzquierdo.x = Input.GetAxis("Horizontal");
        stickIzquierdo.y = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.D) || stickIzquierdo.x > 0f)
        {
            Quaternion targetRotation = Quaternion.Euler(0f, 0f, -inclinacionMaxima);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, velocidadRotacion * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.A) || stickIzquierdo.x < 0f)
        {
            Quaternion targetRotation = Quaternion.Euler(0f, 0f, inclinacionMaxima);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, velocidadRotacion * Time.deltaTime);
        }
        else
        {
            Quaternion targetRotation = Quaternion.Euler(0f, 0f, 0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, velocidadRotacion * Time.deltaTime);
        }
    }
}