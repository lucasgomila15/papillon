using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ControlManagerJugador : MonoBehaviour
{
    ControlDisparoJugador controlDisparoJugador;
    private Rigidbody rb;
    public float vidaJugador;
    public string estado;
    private float fuerza;
    private bool empujar;
    private string tipoPared;
    public bool mejora;
    private float aroPuntos;
    private bool escudo;
    private int jugadorLayer;
    private int escudoLayer;
    public float habilidadNumero;
    public bool canva2Activado = false;



    public GameObject escudoObjeto;
    public GameObject boostParticulas;
    Cinemachine.CinemachineImpulseSource source;

    public TextMeshProUGUI es;
    public TextMeshProUGUI cu;
    public TextMeshProUGUI doo;

    [SerializeField] public float puntaje;
    public float nivel = 0;

    public void Awake()
    {
        jugadorLayer = LayerMask.NameToLayer("Jugador");
        escudoLayer = LayerMask.NameToLayer("Aro");
    }

    void Start()
    {
        controlDisparoJugador = gameObject.GetComponent<ControlDisparoJugador>();
        rb = GetComponent<Rigidbody>();
        nivel = 0;
        mejora = false;
    }

    private void Update()
    {

        if (aroPuntos == 0 && !escudo)
        {
            es.color = Color.grey;
            cu.color = Color.grey;
            doo.color = Color.grey;
        } 
        else if (aroPuntos == 1) {
            es.color = Color.red;
        }
        else if(aroPuntos == 2)
        {
            cu.color = Color.red;
        }
        else if (aroPuntos == 3)
        {
            doo.color = Color.red;
        }

        if (mejora && aroPuntos >= 3)
        {
            escudo = true;
            mejora = false;
            aroPuntos = 0;
            es.color = Color.red;
            cu.color = Color.red;
            doo.color = Color.red;
        }

        if((Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.Joystick1Button3)) && habilidadNumero == 1 && escudo)
        {
            escudoObjeto.gameObject.SetActive(true);
            gameObject.layer = escudoLayer;
            StartCoroutine(HacerAccionYEsperar());
            GestorDeAudio.instancia.ReproducirSonido("SonidoTP");
            es.color = Color.grey;
            cu.color = Color.grey;
            doo.color = Color.grey;
        }

        if ((Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.Joystick1Button3)) && habilidadNumero == 2 && escudo)
        {
            controlDisparoJugador.fuerzaDisparo = 100f;
            StartCoroutine(bala2Esperar());
            GestorDeAudio.instancia.ReproducirSonido("SonidoTP");
            es.color = Color.grey;
            cu.color = Color.grey;
            doo.color = Color.grey;
        }

        if ((Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.Joystick1Button3)) && habilidadNumero == 3 && escudo)
        {
            vidaJugador = 100;
            GestorDeAudio.instancia.ReproducirSonido("SonidoTP");
            es.color = Color.grey;
            cu.color = Color.grey;
            doo.color = Color.grey;
        }

        if (vidaJugador <= 0)
        {
            estado = "Muerto";
            escudoObjeto.gameObject.SetActive(false);
            gameObject.layer = jugadorLayer;
            escudo = false;
        }

        if(empujar && tipoPared == "ParedIzquierda")
        {
            rb.AddForce(Vector3.right * fuerza, ForceMode.Impulse);
            empujar = false;
        }
        if (empujar && tipoPared == "ParedDerecha")
        {
            rb.AddForce(Vector3.left * fuerza, ForceMode.Impulse);
            empujar = false;
        }
        if (empujar && tipoPared == "Fondo")
        {
            rb.AddForce(Vector3.up * fuerza, ForceMode.Impulse);
            empujar = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            movimientoCamara();
            vidaJugador -= 80;
        }
        if (collision.gameObject.CompareTag("InstaKill"))
        {
            vidaJugador = 0;
        }
        if (collision.gameObject.CompareTag("Bala"))
        {
            movimientoCamara();
            vidaJugador -= 10;
        }
        if (collision.gameObject.CompareTag("Cohete"))
        {
            vidaJugador -= 50;
        }
        if (collision.gameObject.CompareTag("Aro"))
        {
            StartCoroutine(boostEsperar());
            puntaje += 1;
            aroPuntos += 1;
            GestorDeAudio.instancia.ReproducirSonido("SonidoAro");
        }
        if (collision.gameObject.CompareTag("AroVida"))
        {
            vidaJugador += 10;
        }
        if (collision.gameObject.CompareTag("AroFinal"))
        {
            aroPuntos = 0;
            puntaje += 1;
            nivel += 1;
            escudoObjeto.gameObject.SetActive(false);
            gameObject.layer = jugadorLayer;
            escudo = false;
        }
        if (collision.gameObject.CompareTag("AroReinicio"))
        {
            aroPuntos = 0;
            mejora = false;
        }

        if (collision.gameObject.CompareTag("ParedIzquierda"))
        {
            aroPuntos = 0;
            tipoPared = "ParedIzquierda";
            empujar = true;
            vidaJugador -= 40;
            fuerza = 300;
        }
        if (collision.gameObject.CompareTag("ParedDerecha"))
        {
            aroPuntos = 0;
            tipoPared = "ParedDerecha";
            empujar = true;
            vidaJugador -= 40;
            fuerza = 300;
        }
        if (collision.gameObject.CompareTag("Fondo"))
        {
            aroPuntos = 0;
            tipoPared = "Fondo";
            empujar = true;
            vidaJugador -= 40;
            fuerza = 300;
        }
    }

    IEnumerator HacerAccionYEsperar()
    {
        yield return new WaitForSeconds(5f);

        escudoObjeto.gameObject.SetActive(false);
        gameObject.layer = jugadorLayer;
        escudo = false;
    }
    IEnumerator bala2Esperar()
    {
        yield return new WaitForSeconds(5f);

        controlDisparoJugador.fuerzaDisparo = 50f;

        escudo = false;
    }
    IEnumerator boostEsperar()
    {
        boostParticulas.gameObject.SetActive(true);

        yield return new WaitForSeconds(3f);

        boostParticulas.gameObject.SetActive(false);
    }
    public void movimientoCamara()
    {
        source = GetComponent<Cinemachine.CinemachineImpulseSource>();

        source.GenerateImpulse(Camera.main.transform.forward);
    }
}
